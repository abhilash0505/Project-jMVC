/*
Copyright (c) 2011 Abhilash Gopal

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/


//Controls the pagination and Navigation of the site and not the View
window.MVC.addController((function( $, MVC ){

	function Controller(){
		//Add more vars
		this.stamp = "controller: ";
	};

	// Extend the core MVC controller (REQUIRED).
	Controller.prototype = new MVC.Controller();
	
	Controller.prototype.init = function(){
		//Get the Model and View
		var model = window.MVC.getModel( "appModel" );
		var	view = window.MVC.getView( "appView" );
		
		alert(this.stamp+"Hello from Controller");
		model.showAlert("Hello from Model");
		view.showAlert("Hello from View");
		
		MVC.log("MVC Log prototype");
	};
	
	
	// ----------------------------------------------------------------------- //
	// ----------------------------------------------------------------------- //
	
	
	
	
	
	// ----------------------------------------------------------------------- //
	// ----------------------------------------------------------------------- //
	
	// Return a new contoller instance.
	return( new Controller() );
	
})( jQuery, window.MVC ));
